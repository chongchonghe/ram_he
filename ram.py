#!/usr/bin/env python
"""This script takes stadard RAMSES movie outputs
   and creates movie with ffmpeg"""
import sys
import os
import time
import logging
import subprocess
import warnings
from argparse import ArgumentParser
import multiprocessing as mp
import matplotlib
try:
    matplotlib.use('agg')
except ValueError:
    pass
from matplotlib import pyplot as plt
import matplotlib.patches as mpatches
from matplotlib.collections import PatchCollection
from numpy.polynomial.polynomial import polyfit
import numpy
from scipy import signal
from pyramid import load_namelist
from pyramid import load_units, load_sink, load_map, load_times, load_info_rt
from pyramid import C

warnings.filterwarnings("error")

#bin_ffmpeg = "ffmpeg"
bin_ffmpeg = "/usr/local/Cellar/ffmpeg/3.4.2/bin/ffmpeg"


def a2z(aexp):
    """Converts expansion factor to redshift"""
    return 1./aexp-1.


def make_image(i, args, nml, cmin, cmax, all_sinks, all_times, units, crvir):
    """Makes one image made up of specified projections"""

    if not os.path.exists("%s/movie1/info_%05d.txt" % (args.dir, i)):
        return

    if args.debug:
        logging.info(' Working on frame: %d' % i)

    outfile = ("{}/pngs/{}_{:05d}.png"
                   .format(args.dir, args.config_file.split('/')[-1],
                                          (i-int(args.fmin))/args.step))

    if args.keep and os.path.exists(outfile):
        # image is there
        if os.stat(outfile).st_mtime > os.stat(args.config_file).st_mtime:
            # and it's newer than config
            return

    nx = nml['movie_params']['nw_frame']
    ny = nml['movie_params']['nh_frame']
    boxlen = nml['amr_params']['boxlen']

    len_proj_axis = len(nml['movie_params']['proj_axis'])
    centre_frame = numpy.zeros((3, 4*len_proj_axis))
    delta_frame = numpy.zeros((3, 2*len_proj_axis))

    ax_code = 'xyz'
    for v in xrange(0, 3):
        for p in xrange(len_proj_axis):  # set default of frame
            centre_frame[v][4*p] = boxlen/2.
            delta_frame[v][2*p] = boxlen
        cf_axis = nml['movie_params'][ax_code[v]+'centre_frame']
        df_axis = nml['movie_params']['delta'+ax_code[v]+'_frame']
        centre_frame[v] = cf_axis[0:len(centre_frame[v])]
        delta_frame[v] = df_axis[0:len(delta_frame[v])]
    del ax_code, cf_axis, df_axis, len_proj_axis

    fig = plt.figure(frameon=False)
    fig.set_size_inches(nx/100.*args.geometry[1],
                        ny/100.*args.geometry[0])
    if args.debug:
        logging.info(' Created figure')

    # need to load units here due to cosmo
    if nml['run_params']['cosmo']:
        units = load_units(args.dir, i, 1)

    if nml['run_params']['rt']:
        if 'Fp' in args.kind[p]:
            # load rt parameters to convert photon densities into units of Kelvin
            groupE = load_info_rt(args.dir, i, 1)  # groupE: [eV]
            groupE = numpy.array(groupE)*C['eV2erg']  # eV to ergs
            c_times_a = C['c']*C['a_rad']*units.v  # speed of light in cgs times radiation constant in g/cm/s^2/K^4

    # plot_sinks = all_sinks[:i-args.fmin]
    # plot_times = all_times[:i-args.fmin]

    filt_factor = [1.00/0.11, 0.25/0.36, 0.5/0.49]  # SDSS ugr filter factors

    if args.debug:
        logging.info(' Starting loop over %d projections' % (len(args.proj)))

    for p in xrange(len(args.proj)):

        proj = args.proj[p]-1
        axis = nml['movie_params']['proj_axis'][proj]

        # calculate image extent
        frame_centre_w = 0.0
        frame_centre_h = 0.0
        frame_centre_d = 0.0
        frame_delta_w = 0.0
        frame_delta_h = 0.0
        frame_delta_d = 0.0

        a = all_times[(i-int(args.fmin))/args.step]

        if axis == 'x':
            w = 1
            h = 2
            d = 0
        elif axis == 'y':
            w = 0
            h = 2
            d = 1
        else:
            w = 0
            h = 1
            d = 2

        for k in xrange(0, 4):
            frame_centre_w += (centre_frame
                               [w][4*proj+k]*a**k)
            frame_centre_h += (centre_frame
                               [h][4*proj+k]*a**k)
            frame_centre_d += (centre_frame
                               [d][4*proj+k]*a**k)

            if k < 2 and a**k != 0.:
                frame_delta_w += (delta_frame
                                  [w][2*proj+k]/a**k)
                frame_delta_h += (delta_frame
                                  [h][2*proj+k]/a**k)
                frame_delta_d += (delta_frame
                                  [d][2*proj+k]/a**k)

        px_in_cm = frame_delta_w/float(nx)*units.l

        # load data
        dat = load_map(args, p, i)
        dat = numpy.array(dat, dtype=numpy.float64)
        if args.debug:
            logging.info(' Data loaded: %d %d' % (p, i))

        if args.label[p] != '':  # load map_unit from label
            map_unit = args.label[p].translate(None, '(){}[]<>').split()[-1]
        else:  # set defaults
            if args.kind[p] == 'dens':
                map_unit = 'H/cc'
            elif args.kind[p] in ['dm', 'stars']:
                map_unit = 'Msun'
            elif args.kind[p] == 'temp':
                map_unit = 'K'
            elif args.kind[p] in ['pres', 'pmag']:
                map_unit = 'barye'
            elif args.kind[p] in ['stars', 'dm']:
                map_unit = 'Msun/pc2'
            else:
                map_unit = ''

        if args.kind[p] == 'dens' and map_unit == 'H/cc':
            dat *= units.d/C['m_H']  # in H/cc
        if args.kind[p] == 'dens' and map_unit == 'g/cc':
            dat *= units.d  # in g/cc
        if args.kind[p] == 'dens' and map_unit == 'Msun/pc3':
            dat *= units.d/(C['Msun']/C['pc2cm']**3)  # in Msun/pc3
        if args.kind[p] == 'temp' and map_unit == 'keV':
            dat *= C['k_B']/C['eV2erg']/1.e3  # in keV
        if args.kind[p] in ["vx", "vy", "vz"]:
            dat *= (units.l/units.t)/1.e5  # in km/s
        if args.kind[p] == 'var6':  # metals are stored as var6, usually
            dat /= 0.02  # in Z_solar
        if args.kind[p] in ['pmag', 'pres'] and map_unit == 'barye':
            dat *= units.d*units.v**2  # in barye
        if args.kind[p] in ['dm', 'stars'] and map_unit == 'Msun/pc2':
            dat *= units.inMsun()  # in Solar mass
            dat /= (px_in_cm/C['pc2cm'])**2
        if args.kind[p] in ['dm', 'stars'] and map_unit == 'g/cm2':
            dat *= units.m
            dat *= px_in_cm**2

        if nml['run_params']['rt']:
            if 'Fp' in args.kind[p] and map_unit == 'K':
                dat *= float(groupE[int(args.kind[p][-1])-1])/(c_times_a)
                dat = numpy.power(dat, 0.25)

        # Reshape data to 2d
        dat = dat.reshape(ny, nx)

        # PSF convolution
        if args.kind[p] in ['stars', 'dm', 'filter1', 'filter2', 'filter3'] and args.smooth > 0:
            kernel = numpy.outer(signal.gaussian(100, args.smooth),
                                 signal.gaussian(100, args.smooth))
            dat = signal.fftconvolve(dat, kernel, mode='same')

        if args.kind[p:-1] == 'filter':
            dat *= 1e16
            dat *= filt_factor[int(args.kind[-1])-1]

        # Log scale?
        # never logscale for velocities
        if(args.logscale[p] and args.kind[p] not in ["vx", "vy", "vz"]):
            try:
                if numpy.all(dat <= 0.0):
                    dat[:, :] = numpy.log10(numpy.finfo(numpy.float32).tiny)
                else:
                    dat[dat <= 0.0] = numpy.nan  # log10 does not crash with nans
                    dat[numpy.where(numpy.isnan(dat))] = numpy.nanmin(dat)  # replace nans with min
                    dat = numpy.log10(dat)
                cbar_format = '%.1f'
            except RuntimeWarning:  # catching and passing
                print 'Careful! Problem with logarithms! kind={}'.format(args.kind[p])
        else:
            dat[numpy.where(numpy.isnan(dat))] = numpy.nanmin(dat)
            cbar_format = '%.0e'

        if args.debug:
            logging.info(' Data logscaled')

        rawmin = numpy.amin(dat)
        rawmax = numpy.amax(dat)
        bg = numpy.where(dat == 0)  # outside of the box

        # Bounds
        if args.min[p] == "" or args.min[p][0] == 'p':
            plotmin = rawmin
        else:
            plotmin = float(args.min[p])
            if(args.logscale[p] and args.kind[p] not in ["vx", "vy", "vz"]):
                plotmin = numpy.log10(plotmin)
            dat[dat < plotmin] = plotmin

        if args.max[p] == "" or args.max[p][0] == 'p':
            plotmax = rawmax
        else:
            plotmax = float(args.max[p])
            if(args.logscale[p] and args.kind[p] not in ["vx", "vy", "vz"]):
                plotmax = numpy.log10(plotmax)
            dat[dat > plotmax] = plotmax

        # Polynomial limiting
        if len(args.min[p]) > 0:
            if args.min[p][0] == 'p':
                p_min = 0.
                for d in xrange(int(args.min[p][-1])+1):
                    p_min += cmin[p][d]*i**d
                plotmin = p_min

        if len(args.max[p]) > 0:
            if args.max[p][0] == 'p':
                p_max = 0.
                for d in xrange(int(args.max[p][-1])+1):
                    p_max += cmax[p][d]*i**d
                plotmax = p_max

        dat[bg] = plotmin

        # set middle of the scale at 0
        if args.kind[p] in ["vx", "vy", "vz"]:
            plotmax = max(abs(plotmin), plotmax)
            plotmin = -plotmax

        rollby = [0, 0]
        if args.roll_axis == "x":
            rollby[0] = nx/2
            dat = numpy.roll(dat, rollby[0], axis=1)
        if args.roll_axis == "y":
            rollby[1] = ny/2
            dat = numpy.roll(dat, rollby[1], axis=0)

        if args.debug:
            logging.info(' Building plots!')
        ############
        # Plotting #
        ############

        ax = fig.add_subplot(args.geometry[0], args.geometry[1], p+1)
        ax.axis([0, nx, 0, ny])
        fig.add_axes(ax)

        if args.kind[p] != 'plot':
            # show image
            image = ax.imshow(dat, interpolation='none', cmap=args.colormap[p],
                              vmin=plotmin, vmax=plotmax, aspect='auto')
            # remove ticks
            ax.tick_params(bottom='off', top='off', left='off', right='off')
            ax.tick_params(labelbottom='off', labeltop='off',
                           labelleft='off', labelright='off')

            if args.colorbar[p]:  # add colorbar
                cbaxes = fig.add_axes([1./args.geometry[1] +
                                       p % args.geometry[1] *
                                       1./args.geometry[1] -
                                       args.cbar_width / args.geometry[1],
                                       abs(p-args.geometry[0] *
                                           args.geometry[1]+1) /
                                       args.geometry[1] *
                                       1./args.geometry[0],
                                       args.cbar_width / args.geometry[1],
                                       1./args.geometry[0]])
                cbar = plt.colorbar(image, cax=cbaxes, format=cbar_format,
                                    ticks=numpy.linspace(plotmin+0.1*(plotmax-plotmin),
                                                         plotmax-0.1*(plotmax-plotmin),
                                                         5))
                cbar.solids.set_rasterized(True)
                cbar.ax.tick_params(width=0, labeltop='on',
                                    labelcolor=args.labelcolor[p],
                                    labelsize=args.fontsize[p], pad=-5)
                for tick in cbar.ax.get_yticklabels():
                    tick.set_horizontalalignment('right')

            # add streamlines from magnetic field lines
            if args.kind[p] == 'pmag' and args.streamlines[p] is True:
                if axis == 'x':
                    dat_U = load_map(args, p, i, 'byl').reshape(ny, nx)
                    dat_V = load_map(args, p, i, 'bzl').reshape(ny, nx)
                elif axis == 'y':
                    dat_U = load_map(args, p, i, 'bxl').reshape(ny, nx)
                    dat_V = load_map(args, p, i, 'bzl').reshape(ny, nx)
                elif axis == 'z':
                    dat_U = load_map(args, p, i, 'bxl').reshape(ny, nx)
                    dat_V = load_map(args, p, i, 'byl').reshape(ny, nx)
                pX = numpy.linspace(0, nx, num=nx, endpoint=False)
                pY = numpy.linspace(0, ny, num=ny, endpoint=False)
                ax.streamplot(pX, pY, dat_U, dat_V, density=0.25,
                              color=args.labelcolor[p], linewidth=1.0)

            if args.sink_flags[p]:
                # camera basics
                if not nml['run_params']['cosmo']:
                    aendmov = float(nml['movie_params']['tendmov'])
                    astartmov = float(nml['movie_params']['tstartmov'])
                else:
                    aendmov = float(nml['movie_params']['aendmov'])
                    astartmov = float(nml['movie_params']['astartmov'])

                # r_sink = 1/2**(level_max) * ir_cloud(default=4) * boxlen;
                # then convert into pts
                r_sink = (0.5**(nml['amr_params']['levelmax'])*4.*boxlen*nx /
                          frame_delta_w*1.5)

                sinks = all_sinks[(i-int(args.fmin))/args.step]
                if nml['run_params']['sink'] and sinks is not None:
                    if args.true_sink[p]:
                        area_sink = r_sink*r_sink
                    else:
                        #area_sink = 100
                        area_sink = args.area_sink[p]

                    aexp = all_times[(i-int(args.fmin))/args.step]  # works for non-cosmo too!

                    theta_cam = (nml['movie_params']['theta_camera'][proj]*numpy.pi/180. +
                                 numpy.min([numpy.max([aexp-nml['movie_params']['tstart_theta_camera'][proj], 0.]), aendmov]) *
                                 nml['movie_params']['dtheta_camera'][args.proj[p]-1]*numpy.pi/180. /
                                 (aendmov-astartmov))
                    phi_cam = (nml['movie_params']['phi_camera'][proj]*numpy.pi/180. +
                                 numpy.min([numpy.max([aexp-nml['movie_params']['tstart_phi_camera'][proj], 0.]), aendmov]) *
                               nml['movie_params']['dphi_camera'][proj]*numpy.pi/180. /
                               (aendmov-astartmov))
                    dist_cam = (nml['movie_params']['dist_camera'][proj] +
                                numpy.min([numpy.max([aexp-nml['movie_params']['tstart_theta_camera'][proj], 0.]),
                                           nml['movie_params']['tend_theta_camera'][proj]]) *
                                nml['movie_params']['ddist_camera'][proj] /
                                (aendmov-astartmov))
                    focal_cam = nml['movie_params']['focal_camera'][proj]
                    if focal_cam <= 0. or focal_cam > nml['movie_params']['dist_camera'][proj]:
                        focal_cam = dist_cam

                    for sink in sinks:
                        xs = sink.pos[w]-frame_centre_w/boxlen
                        ys = sink.pos[h]-frame_centre_h/boxlen
                        zs = sink.pos[d]-frame_centre_d/boxlen
                        xtmp = numpy.cos(theta_cam)*xs+numpy.sin(theta_cam)*ys
                        ytmp = numpy.cos(theta_cam)*ys-numpy.sin(theta_cam)*xs
                        xs = xtmp
                        ys = ytmp
                        ytmp = numpy.cos(phi_cam)*ys+numpy.sin(phi_cam)*zs
                        ztmp = numpy.cos(phi_cam)*zs-numpy.sin(phi_cam)*ys
                        ys = ytmp
                        zs = ztmp
                        del xtmp, ytmp, ztmp
                        if nml['movie_params']['perspective_camera'][proj]:
                            xs *= focal_cam/(dist_cam-zs)
                            ys *= focal_cam/(dist_cam-zs)

                        # Plotting sink if fits in depth
                        if abs(zs) < frame_delta_d/boxlen/2.:
                            ax.scatter(xs/(frame_delta_w/boxlen/2.)*nx/2 +
                                       nx/2-rollby[0],
                                       ys/(frame_delta_h/boxlen/2.)*ny/2 +
                                       ny/2-rollby[1],
                                       marker='o',
                                       #marker='*',
                                       #marker=args.sink_marker,
                                       c='w',
                                       #facecolor='none',
                                       #edgecolor=args.labelcolor[p],
                                       s=area_sink)  # s takes area in pts
                                       #,lw=2)
                        del xs, ys, zs

            # plot rvir circle if measured
            if nml['run_params']['cosmo'] and len(args.aexp_rvir_file):
                max_index = numpy.unravel_index(dat.argmax(), dat.shape)
                aexp = all_times[(i-int(args.fmin))/args.step]
                if aexp > 0.15:  # limit the plotting to higher aexp
                    ax.scatter(max_index[1], max_index[0], marker='o',
                               facecolor='none', lw=2, alpha=0.5,
                               edgecolor=args.labelcolor[p],
                               s=((crvir[0]+crvir[1]*aexp +
                                   crvir[2]*aexp**2+crvir[3]*aexp**3) *
                                  args.scale_l[p]*nx /
                                  (float(boxlen)*units.l*3.24e-19 *
                                  frame_delta_w/float(boxlen))*1000*0.6777))

            patches = []
            if len(args.bar[p]) > 0:
                barlen, barunit = args.bar[p].split(" ")
                barlen = int(barlen)
                if barunit == 'AU':
                    px_in_user = px_in_cm/1.496e+13
                elif barunit == 'pc':
                    px_in_user = px_in_cm/3.086e18
                elif barunit == 'kpc':
                    px_in_user = px_in_cm/3.086e21
                else:  # Mpc
                    px_in_user = px_in_cm/3.086e24

                if barlen > 0:
                    barlen_px = barlen/px_in_user
                    if barlen_px < 100:
                        barlen_px = 100
                        barlen = int(barlen_px*px_in_user)
                    rect = mpatches.Rectangle((0.025*nx, 0.025*ny),
                                              barlen_px, 10)

                    ax.text(0.025+float(barlen_px/nx/2),
                            0.025+15./ny, "%d %s" % (barlen, barunit),
                            verticalalignment='bottom',
                            horizontalalignment='center',
                            transform=ax.transAxes,
                            color=args.labelcolor[p],
                            fontsize=args.fontsize[p])

                    patches.append(rect)

            # add timer to plot
            if args.timer[p]:
                if nml['run_params']['cosmo']:
                    # aexp instead of time
                    aexp = all_times[(i-int(args.fmin))/args.step]
                    ax.text(0.05, 0.95, 'z={a:.2f}'.format(a=abs(a2z(aexp))),
                            verticalalignment='bottom',
                            horizontalalignment='left',
                            transform=ax.transAxes,
                            color=args.labelcolor[p],
                            fontsize=args.fontsize[p])
                else:
                    t = all_times[(i-int(args.fmin))/args.step]
                    t *= units.t/86400/365.25  # time in years
                    time_print_set = '%.{}f %s'.format(1)
                    if t >= 1e3 and t < 1e6:
                        scale_t = 1e3
                        t_unit = 'kyr'
                    elif t > 1e6 and t < 1e9:
                        scale_t = 1e6
                        t_unit = 'Myr'
                    elif t > 1e9:
                        scale_t = 1e9
                        t_unit = 'Gyr'
                        time_print_set = '%.{}f %s'.format(2)
                    else:
                        scale_t = 1.
                        t_unit = 'yr'

                    ax.text(0.05, 0.95, time_print_set % (t/scale_t, t_unit),
                            verticalalignment='bottom',
                            horizontalalignment='left',
                            transform=ax.transAxes,
                            color=args.labelcolor[p],
                            fontsize=args.fontsize[p])

            if args.label[p] != "":  # adding projection label
                ax.text(0.85, 0.95, '%s' % (args.label[p]),
                        verticalalignment='bottom',
                        horizontalalignment='right',
                        transform=ax.transAxes,
                        color=args.labelcolor[p], fontsize=args.fontsize[p])

            collection = PatchCollection(patches, facecolor=args.labelcolor[p])
            ax.add_collection(collection)

    # corrects window extent
    plt.subplots_adjust(left=0., bottom=0.,
                        right=1.+1.0/nx, top=1.+1.0/ny,
                        wspace=0., hspace=0.)
    try:
        plt.savefig(outfile, dpi=100)
        if args.debug:
            logging.info(' Figure successfully saved!')
    except DeprecationWarning:  # just a comparison warning
        pass
    plt.close(fig)

    return


def fit_min_max(args, p):
    """Fit polynomials to min and max of a given map"""
    mins = numpy.array([])
    maxs = numpy.array([])

    for i in xrange(args.fmin, args.fmax+1, args.step):
        dat = load_map(args, p, i)
        dat = numpy.array(dat, dtype=numpy.float64)
        if len(dat) == 0:
            mins = numpy.append(mins, mins[-1])
            maxs = numpy.append(maxs, maxs[-1])
            continue

        # unit loading must stay here for cosmo runs
        units = load_units(args.dir, i, args.proj[p])
        if args.kind[p] == 'dens':
            dat *= units.d/1.6737236e-24    # in H/cc
        if args.kind[p] in ['vx', 'vy', 'vz']:
            dat *= (units.l/units.t)/1e5  # in km/s
        if args.kind[p] in ['pres', 'pmag']:
            dat *= units.d*units.v**2  # in barye

        if args.logscale[p]:
            try:
                if numpy.all(dat <= 0.0):
                    datmin = numpy.finfo(numpy.float32).tiny
                    datmax = numpy.finfo(numpy.float32).tiny
                else:
                    dat[dat <= 0.0] = numpy.nan
                    datmin = numpy.nanmin(dat)
                    datmax = numpy.nanmax(dat)
                mins = numpy.append(mins, numpy.log10(datmin))
                maxs = numpy.append(maxs, numpy.log10(datmax))
            except RuntimeWarning:
                print 'Log problem in poly fitting for {}'.format(args.kind[p])
        else:
            mins = numpy.append(mins, numpy.amin(dat))
            maxs = numpy.append(maxs, numpy.amax(dat))

    indexes = range(args.fmin, args.fmax+1, args.step)

    cmin = polyfit(indexes, mins, args.poly[0])
    cmax = polyfit(indexes, maxs, args.poly[1])

    return p, cmin, cmax


def interpol_rvir(args):
    """Cosmo only. Given list of aexp and rvir,
    it interpolates the rvir for plotting in the movie"""

    aexp, rvir = numpy.loadtxt(args.dir+'/'+args.aexp_rvir_file).T

    crvir = polyfit(aexp, rvir, 3, full=True)[0]

    return crvir


def main():
    """Main body - parses arguments, create images and executes ffmpeg"""
    __version__ = "1.16.1"
    advert = """\
             _____ _____ _____
            | __  |  _  |     |
            |    -|     | | | |
            |__|__|__|__|_|_|_|

          RAMSES Animation Maker
   v {version} - 2017/09/12 - P. Biernacki
    """.format(version=__version__)
    print advert
    # Check if running python2
    if sys.version_info.major != 2:
        print "This script works only in python2, sorry!"
        sys.exit()
    # Parse command line arguments
    parser = ArgumentParser(description="Script to create RAMSES movies")
    parser.add_argument("-c", "--config", dest="config", metavar="VALUE",
                        type=str, help="config file  [%(default)s]",
                        default="./config.ini")
    parser.add_argument("-d", "--debug", dest="debug", action='store_true',
                        help="debug mode [%(default)r]",
                        default=False)
    parser.add_argument("-n", "--ncpu", dest="ncpu", metavar="VALUE",
                        type=int, help="number of CPUs for multiprocessing [%(default)d]",
                        default=1)
    parser.add_argument("-o", "--output", dest="outfile", metavar="FILE",
                        type=str, help="output image file [<map_file>.png]",
                        default=None)

    for k in xrange(len(sys.argv)):
        if sys.argv[k] in ["-c", "--config"]:
            config_file = sys.argv[k+1]
            break
        else:
            config_file = "./config.ini"

    from load_settings import load_settings
    args = load_settings(parser, config_file)
    if args.debug:
        logging.basicConfig(filename='m2m_debug.log', level=logging.INFO)
        logging.info('Command line: ' % sys.argv)

    # load basic info once, instead of at each loop
    nml = load_namelist(args.namelist, args.dir)
    if args.debug:
        logging.info('Namelist parameters loaded.')

    # load units if not cosmo
    if not nml['run_params']['cosmo']:
        units = load_units(args.dir, args.fmax, 1)
    else:
        units = None

    # Progressbar imports
    try:
        from widgets import Percentage, Bar, ETA
        from progressbar import ProgressBar
        progressbar_avail = True
    except ImportError:
        progressbar_avail = False

    if args.debug:
        progressbar_avail = False

    if args.debug:
        logging.info('Progressbar availibility: ' % int(progressbar_avail))

    # for each projection fit mins and maxs with polynomial
    cmins = numpy.zeros(len(args.proj)*6).reshape(len(args.proj), 6)
    cmaxs = numpy.zeros(len(args.proj)*6).reshape(len(args.proj), 6)

    for p in xrange(len(args.proj)):
        if len(args.min[p]) > 0 and len(args.max[p]) > 0:
            if (args.min[p][0] == 'p') or (args.max[p][0] == 'p'):
                args.poly = [int(args.min[p][-1]), int(args.max[p][-1])]
                if args.ncpu > 1:
                    results = []
                    pool = mp.Pool(processes=min(args.ncpu, len(args.proj)))

                    results.append(pool.apply_async(fit_min_max,
                                                    args=(args, p,)))
                    pool.close()
                    pool.join()
                    output = [w.get() for w in results]

                    # just for safety if executed not in order
                    for w in xrange(len(output)):
                        for d in xrange(len(args.proj)):
                            if output[w][0] == d:
                                cmins[d][0:args.poly[0]+1] = output[w][1]
                                cmaxs[d][0:args.poly[1]+1] = output[w][2]

                elif args.ncpu == 1:
                    (temp,
                     cmins[p][0:args.poly[0]+1],
                     cmaxs[p][0:args.poly[1]+1]) = fit_min_max(args, p)

                else:
                    print 'Wrong number of CPUs! Exiting!'
                    sys.exit()

                print 'Polynomial coefficients fitted!'

    # load sinks
    all_sinks = []
    all_times = []

    for i in xrange(args.fmin, args.fmax+1, args.step):
        if True in args.sink_flags:
            sinks_i = load_sink(args.dir, i, nml['amr_params']['boxlen'])
            all_sinks.append(sinks_i)
        time_i = load_times(args.dir, i, nml['run_params']['cosmo'])
        all_times.append(time_i)
    if args.debug:
        logging.info('Sinks loaded')

    # interpolating rvir
    crvir = 0
    if len(args.aexp_rvir_file):
        crvir = interpol_rvir(args)

    # creating images
    if progressbar_avail:
        widgets = ['Working...', Percentage(), Bar(marker='#'), ETA()]
        pbar = ProgressBar(widgets=widgets, maxval=(args.fmax-args.fmin)/args.step+1).start()
    elif args.debug:
        print '-> Debugging mode'
    else:
        print 'Working!'

    if not os.path.exists("%s/pngs/" % (args.dir)):
        os.makedirs("%s/pngs/" % (args.dir))

    if args.ncpu > 1:
        if args.debug:
            logging.info('Running in multicore mode')
        results = []
        pool = mp.Pool(processes=args.ncpu)
        for i in xrange(args.fmin, args.fmax+1, args.step):
            results.append(pool.apply_async(make_image,
                                            args=(i, args,
                                                  nml, cmins, cmaxs,
                                                  all_sinks, all_times, units,
                                                  crvir,
                                                 )))
        while True:
            inc_count = sum(1 for x in results if not x.ready())
            if inc_count == 0:
                break
            if progressbar_avail:
                pbar.update((args.fmax-args.fmin)/args.step+1-inc_count)
            time.sleep(.1)

        pool.close()
        pool.join()

    elif args.ncpu == 1:
        if args.debug:
            logging.info('Running in single mode')
        for i in xrange(args.fmin, args.fmax+1, args.step):
            make_image(i, args, nml,
                       cmins, cmaxs, all_sinks, all_times, units, crvir)
            if progressbar_avail:
                pbar.update((i+1-args.fmin)/args.step)

    else:
        print 'Wrong number of CPUs! Exiting!'
        sys.exit()

    if progressbar_avail:
        pbar.finish()

    # movie name for montage
    frame = "{dir}/pngs/{name}_%*.png".format(dir=args.dir,
                                              name=args.config_file.split('/')[-1])
    mov = "{dir}/multi.mp4".format(dir=args.dir)

    if args.fname != "":
        mov = "{dir}/{fname}".format(dir=args.dir, fname=args.fname)

    if args.debug:
        logging.info('Saving movie to: ', mov)

    ffmpeg_vr_flag = 'h264'
    if args.vr:
        ffmpeg_vr_flag = '-vcodec libx264 -x264opts "frame-packing=3:frame-packing-interpret=1:frame-packing-quincunx=0:frame-packing-grid=0,0,0,0"'
    if args.debug:
        logging.info('ffmpeg flag:', ffmpeg_vr_flag)

    print "Calling ffmpeg! Output: {mov}".format(mov=mov)
    subprocess.call("{binffmpeg} -loglevel quiet -i {input}\
                     -y -vcodec {ffmpeg_vr_flag} -pix_fmt yuv420p\
                     -r {fps} -filter:v 'setpts={speed}*PTS'\
                     {output}".
                    format(binffmpeg=bin_ffmpeg, input=frame,
                           ffmpeg_vr_flag=ffmpeg_vr_flag,
                           fps=args.fps, speed=30./float(args.fps),
                           output=mov), shell=True)
    print "Movie created! Cleaning up!"
    if not args.keep:
        subprocess.call("rm -r {dir}/pngs".format(dir=args.dir), shell=True)
    subprocess.call("chmod a+r {mov}".format(mov=mov), shell=True)
    if args.debug:
        logging.info('Done!')

if __name__ == '__main__':
    main()
